package com.my.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class EnvController {
private static final Logger log = LoggerFactory.getLogger(EnvController.class);

	@GetMapping("/status")
	public String getStatus() {
     log.debug("Hello you are seeing this status");
		return "Hello you are seeing this status";

	}

}